﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace apifirstry
{
    public class Self
    {
        public string Href { get; set; }
    }

    public class Fixtures
    {
        public string Href { get; set; }
    }

    public class Players
    {
        public string Href { get; set; }
    }

    public class Links
    {
        public Self Self { get; set; }
        public Fixtures Fixtures { get; set; }
        public Players Players { get; set; }
    }

    public class Teams
    {
        public Links _links { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string ShortName { get; set; }
        public string SquadMarketValue { get; set; }
        public string CrestUrl { get; set; }
    }
}
